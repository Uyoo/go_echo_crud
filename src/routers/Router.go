package routers

import (
	"os"

	controllers "../Controllers"
	"github.com/labstack/echo"
)

func Api() {

	e := echo.New()

	e.GET("/index", controllers.Index)          // 입력된 모든 정보 조회
	e.GET("/show/:id", controllers.Show)        // 해당 id의 정보 조회
	e.POST("/store", controllers.Store)         // 입력한 정보 저장(create)
	e.PUT("/update/:id", controllers.Update)    // 해당 id의 정보 수정
	e.DELETE("/delete/:id", controllers.Delete) // 해당 id의 정보 삭제

	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
