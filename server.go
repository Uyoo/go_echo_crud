package main

import (
	"./src/routers"
	"github.com/joho/godotenv"
)

func main() {

	// env파일에서 정보 가져올 수 있도록 설정
	godotenv.Load()

	// 라우터 Api 함수 호출
	// import 구문에서 변수 따로 선언을 안했다면 마지막 경로 이름으로 변수 자동 생성
	routers.Api()
}
