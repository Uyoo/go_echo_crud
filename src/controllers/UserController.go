package controllers

import (
	"net/http"

	DB "../configs"
	Models "../models"
	"github.com/labstack/echo"
)

var persons []Models.UserModel
var messages map[string]string

func Index(c echo.Context) error {

	// UserModel table에 있는 모든 정보 조회
	rows := DB.Init().Find(&persons)
	return c.JSON(http.StatusOK, rows)
}

func Show(c echo.Context) error {
	id := c.Param("id")

	// UserModel table에서 해당 id에 맞는 정보 조회
	person := DB.Init().Find(&Models.UserModel{}, id)

	return c.JSON(http.StatusOK, person)
}

func Store(c echo.Context) error {
	username := c.FormValue("username")
	email := c.FormValue("email")

	// UserModel table에 입력된 정보 저장
	person := DB.Init().Create(&Models.UserModel{
		Username: username,
		Email:    email,
	})

	return c.JSON(http.StatusOK, person)
}

func Update(c echo.Context) error {
	id := c.Param("id")
	username := c.FormValue("username")
	email := c.FormValue("email")

	// UserModel table에서 해당 id에 맞는 정보 수정
	person := DB.Init().First(&Models.UserModel{}, id).Update(Models.UserModel{
		Username: username,
		Email:    email,
	})

	return c.JSON(http.StatusOK, person)
}

func Delete(c echo.Context) error {
	id := c.Param("id")

	// UserModel table에서 해당 id에 맞는 정보 삭제
	person := DB.Init().Delete(&Models.UserModel{}, id)
	return c.JSON(http.StatusOK, person)
}
