package models

import (
	"github.com/jinzhu/gorm"
)

// db table 구성
type UserModel struct {
	gorm.Model
	Username string `gorm:"unique;not null;type:varchar(20)"`
	Email    string `gorm:"type:varchar(100);unique_index;not null"`
}
